Stuffilike::Application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  devise_for :users ,  controllers: {:omniauth_callbacks => "omniauth_callbacks", registrations: "registrations", sessions: "sessions", passwords: "passwords"}
  resources :users do
    collection do
      post :require_info
      get :admin_user
      get :profile
      get :get_info
    end
  end
  resources :items do
    collection do
      delete :delete_item
      get :get_feature_stores
    end
  end

  post "uploads/photo/:photo_id/:item_id" => 'uploads#photo'
  post "uploads/avatar" => 'uploads#avatar'
  post "uploads/upload_files" => 'uploads#upload_files'
  post "uploads/background" => 'uploads#background'
  post "uploads/photo/:item_id" => 'uploads#photo'
  post "items/photo_update/:item_id" => 'items#photo_update'
  delete 'items/:item_id/delele_photo/:photo_id' => 'items#delele_photo'
  post '/items/:item_id/edit' => 'items#update'
  post '/users/update_avatar' => 'users#update_avatar'
  post '/users/update_background' => 'users#update_background'
  get '/admin' => 'users#admin'
  get '/:username' => 'items#store'
  get '/items/index/:page' => 'items#index'
  get 'items/:page/store/:username' => 'items#store'
  post '/items/add_item_to_store/:item_id' => 'items#add_item_to_store'
  post '/items/remove_item_to_store/:item_id' => 'items#remove_item_to_store'
  root 'home#index'



  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
