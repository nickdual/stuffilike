module ApplicationHelper
  def self.xls_to_json(sheet, cols=nil, colnames=nil, h=true)
    header = sheet.row(1)
    cols = Array.new(header.length){ |index| index } if cols.blank?
    results = []
    h ? row_start = 2 : row_start = 1
    (row_start..sheet.last_row).each do |i|
      row = sheet.row(i); row_modify = {}
      cols.each_with_index do |value, index|
        row_modify[%Q[#{colnames.blank? ? header[value] : colnames[index]}]] = row[value]
      end
      results.push(row_modify)
    end
    return results
  end
end
