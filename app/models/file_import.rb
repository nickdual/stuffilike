class FileImport
  include Mongoid::Document
  field :file_url , :type => String
  field :file, :type => String
  mount_uploader :file, FileUploader

  def furl
    self.file.url.to_s
  end

  def serializable_hash(options={})
    super(options.reverse_merge( :method => [:furl],:except => :file))
  end

end
