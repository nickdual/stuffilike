require 'net/ftp'
class Cj
  class << self
    def sync(file_path)
      client = Client.new
      return if !File.exist?(file_path)
      items = client.get_local_file(file_path, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], ['item', '', 'category', 'merchant', 'brand', 'name', "description", "price", "link", "referral_fee" ], true)
      record_checked = items.length
      items.each do |item|
        params ={
          :item => item["item"],
          :category => item["category"],
          :merchant => item["merchant"],
          :brand => item["brand"],
          :name => item["name"],
          :description => item["description"],
          :price => item["price"],
          :link => item["link"],
          :referral_fee => item["referral_fee"]
        }
        if Item.where(item: item["item"]).exists?
          item = Item.where(item: item["item"]).first
          item.update_attributes(params)
        else
          Item.create(params)
        end
      end
      return record_checked
   end
  end
end