class Avatar
  include Mongoid::Document
  field :avatar_link , :type => String
  field :image, :type => String
  mount_uploader :image, AvatarUploader
  embedded_in :user

  def avatar
    self.image.thumb.to_s
  end

  def serializable_hash(options={})
    super(options.reverse_merge(:methods => [:avatar], :except => :image))
  end

end
