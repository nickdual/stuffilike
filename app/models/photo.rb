class Photo
  include Mongoid::Document
  field :link , :type => String
  field :thumb , :type => String
  field :image, :type => String
  mount_uploader :image, PhotoUploader
  embedded_in :item

  def thumb
    self.image.thumb.to_s
  end

  def url
    self.image.url.to_s
  end

  def serializable_hash(options={})
    super(options.reverse_merge(:methods => [:thumb,:url], :except => :image))
  end

end
