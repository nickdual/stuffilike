class Item
  include Mongoid::Document
  field :name , :type => String
  field :description , :type =>  String
  field :price , :type => BigDecimal
  field :item , :type => String
  field :link , :type => String
  field :referral_fee , :type => Float
  field :merchant , :type => String
  field :category , :type => String
  field :brand , :type => String
  field :store , :type => Boolean , :default => false


  embeds_many :photoes
  has_and_belongs_to_many :users
end
