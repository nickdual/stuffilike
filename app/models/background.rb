class Background
  include Mongoid::Document
  field :background_link , :type => String
  field :image, :type => String
  mount_uploader :image, BackgroundUploader
  embedded_in :user

  def background
    self.image.large_cus.to_s
  end

  def serializable_hash(options={})
    super(options.reverse_merge(:methods => [:background], :except => :image))
  end

end
