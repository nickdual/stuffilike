class User
  include Mongoid::Document
  rolify

  has_many :authentications
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable, :confirmable
  belongs_to :user_role
  has_and_belongs_to_many :items
  embeds_one :avatar
  embeds_one :background

  ## Database authenticatable
  field :email,              :type => String, :default => ""
  field :encrypted_password, :type => String, :default => ""
  
  ## Recoverable
  field :reset_password_token,   :type => String
  field :reset_password_sent_at, :type => Time

  ## Rememberable
  field :remember_created_at, :type => Time

  ## Trackable
  field :sign_in_count,      :type => Integer, :default => 0
  field :current_sign_in_at, :type => Time
  field :last_sign_in_at,    :type => Time
  field :current_sign_in_ip, :type => String
  field :last_sign_in_ip,    :type => String

  ## Confirmable
  field :confirmation_token,   :type => String
  field :confirmed_at,         :type => Time
  field :confirmation_sent_at, :type => Time
  field :unconfirmed_email,    :type => String # Only if using reconfirmable

  ## Token authenticatable
  # field :authentication_token, :type => String
  # info
  field :image_url, :type => String ,:default => "/assets/default_picture.png"
  field :first_name, :type => String
  field :last_name, :type => String
  field :username , :type => String

  #business
  field :charity , :type => String
  validates_uniqueness_of :username
  #some action

  def apply_omniauth(omni)
    authentications.build(:provider => omni['provider'],
                          :uid => omni['uid'],
                          :token => omni['credentials'].token,
                          :token_secret => omni['credentials'].secret)
  end

  def self.find_by_email(email)
    return self.where('email'=> email).first
  end

end
