class PasswordsController < Devise::PasswordsController
  def create
    @user = resource_class.where(params[resource_name])
    if @user.present?
      self.resource = resource_class.send_reset_password_instructions(params[resource_name])
      if successfully_sent?(resource)
        render :json => {success: true}
      else
        render :json => {success: false ,flag: 'sent_false',errors: I18n.t('forgot_password.message_error')}
      end
    else
      render :json => {:success => false, :flag => 'not_email', :errors => t('forgot_password.email_error')}
    end
  end

  def update
    self.resource = resource_class.reset_password_by_token(params[resource_name])
    if resource.errors.empty?
      flash_message = resource.active_for_authentication? ? :updated : :updated_not_active
      set_flash_message(:notice, flash_message) if is_navigational_format?
      sign_in(resource_name, resource)
      redirect_to root_path
    else
      redirect_to root_path
    end
  end
  
end
