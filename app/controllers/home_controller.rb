class HomeController < ApplicationController
  def index
    @item_stores = Item.where('store' =>  true)
    if current_user
      @user = current_user
    else
      @user = nil
    end
  end

end
