class UsersController < ApplicationController
  def index
    if params[:username]
      @user = User.where('username' => params[:username]).first
      respond_to do |format|
        format.html()
        format.js { render :json => @user }
      end
    end
  end

  def update
    if current_user
      @user = current_user
      if @user.update_attributes(update_profile_params)
        render :json => {:user => @user, :status => '200'}
      else
        render :json =>  {:message => @user.errors.full_messages[0].to_s ,:status => '403'}
      end
    end
  end

  def show
    @user = User.find(params[:id])
    render :json => @user
  end

  def require_info
    @user = current_user
    banner_img = params[:require][:banner_img]
    if @user.update_attributes(require_params)
      @background = @user.build_background({'background_link' => banner_img})
      @background.save()
      render :json => {:user => @user, :status => '200'}
    else
      render :json => {:message => @user.errors.full_messages[0].to_s ,:status => '403'}
    end
  end

  def update_avatar
    @avatar = current_user.avatar
    if @avatar
      @avatar.update_attribute('avatar_link', params[:avatar_link])
      render :json => @avatar
    else
      @avatar = current_user.build_avatar({'avatar_link' => params[:avatar_link]})
      if @avatar.save()
        render :json => @avatar
      end
    end
  end

  def update_background
    @background = current_user.background
    if @background
      @background.update_attribute('background_link', params[:background_link])
      render :json => @background
    else
      @background = current_user.build_background({'background_link' => params[:background_link]})
      if @background.save()
        render :json => @background
      end
    end
  end

  def get_info
    if current_user
      @user = current_user
      render :json => @user
    end
  end

  def profile
    if current_user
      respond_to do |format|
         format.html
      end
    else
      redirect_to "/"
    end
  end

  def admin
      @perpage = 20
  end

  def admin_user
    @perpage = 20
    page = params[:page].present? ? params[:page].to_i : 1
    @users = User.all
    @result = @users.limit(@perpage).skip((page - 1) * @perpage)
    total = @users.length
    render :json => {'users' => @result ,'total' => total}
  end

  private

  def profile_params
    params[:profile].delete :_id
    params[:profile].delete :user_id
    params.require(:profile).permit(:first_name, :last_name, :degree_program, :major,:college,:user_id)
  end

  def update_profile_params
    params[:user].delete :_id
    params[:user].delete :avatar
    params[:user].delete :background
    params[:user].delete :item_ids
    params[:user].delete :role_ids
    params[:user].delete :user_role_id
    params[:user].delete :image_url
    params[:user].delete :email
    params.require(:user).permit(:first_name, :last_name, :username, :charity)
  end

  def require_params
    params[:require].permit(:username, :charity)
  end
end
