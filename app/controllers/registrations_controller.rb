class RegistrationsController < Devise::RegistrationsController
  def create
    bg_banner = params[:user][:background_banner]
    @user = User.new(user_params)
    if  @user.save
      @background = @user.build_background({'background_link' => bg_banner})
      @background.save()
      sign_in(@user)
      flash[:notice] = t("users.confirmationsent")
      render :json => {:status => '200'}
    else
      render :json => {:message => @user.errors.full_messages[0].to_s ,:status => '422'}
    end
  end

  private
  def user_params
    params.require(:user).permit(:email, :first_name, :last_name, :password, :password_confirmation,:username,:charity)
  end
end

