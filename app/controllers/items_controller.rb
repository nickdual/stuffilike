require 'net/ftp'
class ItemsController < ApplicationController
  def index
    limit_show = 8
    page = params[:page].present? ? params[:page].to_i : 0
    if current_user
      if current_user.has_role? :admin
        @items = Item.all.limit(limit_show).skip(limit_show * page)
        respond_to do |format|
          format.html()
          format.js { render :json => @items }
        end
      else
        item_ids = current_user.item_ids
        @items = Item.not_in(id: item_ids).limit(limit_show).skip(limit_show * page)
        respond_to do |format|
          format.html()
          format.js { render :json => @items }
        end
      end
    end
  end

  def show
    @item = Item.find(params[:id])
    @self = false
    if @item
      if current_user
        @self = current_user.items.include?(@item)
      end
      render :json => {'item' => @item, 'self' => @self }
    end

  end

  def create
    if params[:item]
      params[:item][:store] = true
      @item = Item.new(item_params)
      if @item.save!()
        render :json =>  @item
      end
    end

  end

  def edit
    if current_user
      if current_user.has_role? :admin
        @item = Item.find(params[:id])
        respond_to do |format|
          format.html()
          format.js { render :json => @item }
        end
      else
        render :json => nil
      end
    end
  end

  def update
    item_id = params[:_id][:$oid]
    @item = Item.find(item_id)
    if @item.update_attributes(item_params)
      render :json => {item: @item, 'status' => true}
    else
      render :json => {'status' => false}
    end
  end

  def destroy
    if params[:id]
      @item = Item.find(params[:id])
      if @item.destroy
        render :json => @item
      else
        render :json => nil
      end

    end
  end

  def add_item_to_store
    @item = Item.find(params[:item_id])
    if @item
      current_user.items << @item
      @items = current_user.items
      render :json => {"status" => true}
    else
      render :json => {"status" => false}
    end
  end

  def remove_item_to_store
    @item = Item.find(params[:item_id])
    if @item
      current_user.items.delete( @item)
      render :json => {"status" => true}
    else
      render :json => {"status" => false}
    end
  end

  def photo_update
    @item = Item.find(params[:item_id])
    if @item
      @photo = @item.photoes.find(params[:_id][:$oid])
      if @photo
        @photo.link = params[:link]
        @photo.thumb = params[:thumb]
        if @photo.save()
          render :json => @photo
        else
          render :json => nil
        end
      else
        render :json => nil
      end
    else
      render :json =>  nil
    end
  end

  def delele_photo
    @item = current_user.items.find(params[:item_id])
    if @item
      @photo = @item.photoes.find(params[:photo_id])
      if @photo
        if @photo.destroy()
          render :json => @photo, :status => 200
        else
          render :json  => nil, :status => 403
        end
      else
        render :json  => nil, :status => 403
      end
    else
      render :json  => nil, :status => 403
    end
  end

  def store
    page = params[:page].present? ? params[:page].to_i : 0
    @username = params[:username]
    @user = User.where('username' => params[:username]).first
    if @user
      @items = @user.items.limit(8).skip(8 * page)
      respond_to do |format|
        format.html
        format.js { render :json => @items}
      end
    end

  end

  def get_feature_stores
    @items = Item.where('store' => true)
    if @items.length > 0
      render :json => @items
    else
      render :json => nil
    end

  end

  private

  def item_params
    params[:item].delete :_id
    params[:item].delete :user_ids
    params.require(:item).permit(:name, :description, :link, :category,:merchant,:referral_fee, :price, :brand, :store)
  end

end
