class UploadsController < ApplicationController

  def photo
    @item = Item.find(params[:item_id])
    if @item
      if params[:photo_id]
        @photo = @item.photoes.find(params[:photo_id])
        if @photo
          @photo.update_attributes(params.require(:photo).permit(:image))
          render :json => @photo
        end
      else
        @photo = @item.photoes.build(params.require(:photo).permit(:image))
        if @photo.save
          render :json => @photo
        end
      end
    end
  end

  def avatar
    @avatar = current_user.avatar
    if @avatar
      @avatar.update_attributes(params.require(:avatar).permit(:image))
      render :json => @avatar
    else
      @avatar = current_user.build_avatar(params.require(:avatar).permit(:image))
      if @avatar.save
        render :json => @avatar
      else
        render :json => nil
      end
    end
  end

  def background
    @background = current_user.background
    if @background
      @background.update_attributes(params.require(:background).permit(:image))
      render :json => @background
    else
      @background = current_user.build_background(params.require(:background).permit(:image))
      if @background.save
        render :json => @background
      else
        render :json => nil
      end
    end
  end

  def upload_files
    param = params.require(:fileData).permit(:file)
    file = FileImport.new(param)
    file.save()
    item_checked = Cj.sync(file.file.path )
    render :json => item_checked

  end
end
