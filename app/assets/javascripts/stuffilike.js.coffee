window.Stuffilike =
  Models: {}
  Collections: {}
  Views: {}
  Routers: {}
  initialize: ->
    console.log 'Hello from Backbone!'
    new Stuffilike.Routers.Items()
    new Stuffilike.Routers.Users()
    header = new Stuffilike.Views.Header({model: Stuffilike.currentUser})
    header.render()
$(document).ready ->
  Stuffilike.initialize()
  Backbone.history.start()
