class Stuffilike.Collections.Items extends Backbone.Collection
  url: '/items'
  model: Stuffilike.Models.Item

  save: () ->
    response = Backbone.sync('update', @, url: @url, contentType: 'application/json', data: JSON.stringify(items: @toJSON()))
    return response

