class Stuffilike.Models.User extends Backbone.Model
  urlRoot: "/users"
  relations:
    "avatar": Backbone.Model
    "background": Backbone.Model

  toJSON: ->
    if @paramRoot
      data = {}
      data[@paramRoot] = _.clone(@attributes);
      return data
    else
      return _.clone(@attributes)

