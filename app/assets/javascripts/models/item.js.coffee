class Stuffilike.Models.Item extends Backbone.Model
  urlRoot: "/items"
  url: '/items'
  relations:
      "photoes": Backbone.Collection

  toJSON: ->
    if @paramRoot
      data = {}
      data[@paramRoot] = _.clone(@attributes);
      return data
    else
      return _.clone(@attributes)

  destroy:() ->
    response = Backbone.sync('delete', @, url: @url, contentType: 'application/json')
    return response
