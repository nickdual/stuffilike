class Stuffilike.Views.MyStore extends Backbone.View
  el: '#store_header'
  template: JST['my_stores/my_store']

  initialize:(opts) ->
    @username = opts.username
    @selfStore = opts.self

  render: ->
    self = @
    if !@selfStore
      $('#top-navigation').find('.nav').children('li.mywarehouse-tab').removeClass('active')
    user = new Stuffilike.Models.User()
    user.url = '/users?username=' + @username
    user.fetch({success:(data) ->
      self.$el.html(self.template({user: user, selfStore: self.selfStore}))
      self.initEditAvatar()
      self.initEditBackground()
    })
    @

  initEditAvatar:() ->
    obj = @
    @$('#edit_avatar').fileupload
      autoUpload: true
      url: 'uploads/avatar'
      start:(e) ->

      done: (e, data) ->
        if data.result
          avatar_link = data.result.avatar
          avatar = Stuffilike.currentUser.get('avatar')
          model = new Backbone.Model()
          model.url = '/users/update_avatar'
          model.set('avatar_link',avatar_link)
          model.save({},{success:(model) ->
            obj.$('.avatar').find('img.ava-image').attr('src', model.get('avatar_link') )
          })

  initEditBackground:() ->
    obj = @
    @$('#edit_background').fileupload
      autoUpload: true
      url: 'uploads/background'
      start:(e) ->

      done: (e, data) ->
        if data.result
          bg_link = data.result.background
          bg =  Stuffilike.currentUser.get('background')
          model = new Backbone.Model()
          model.url = '/users/update_background'
          model.set('background_link', bg_link)
          model.save({},{success:(model) ->
            obj.$('.background').find('img.bg-image').attr('src', model.get('background_link') )
          })