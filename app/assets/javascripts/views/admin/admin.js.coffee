class Stuffilike.Views.Admin extends Backbone.View
  el: '#admin'
  template: JST['admin/user_row']

  events:
    'click .control-panel' : 'controlPanel'
    'click .feature-store' : 'featureStoreTab'
    'click .paginate' :'paginate'
    'click .paginate.end' :'changeUpPaginateNav'
    'click .paginate.start' :'changeDownPaginateNav'

  init: ->
    $('.admin-tab').addClass('active')
    @initUpload()
    @checkFile()

  addOne:(model, index) =>
    page = parseInt($('#list_table_body').attr('data-id'))
    $(@template({user: model, index: ((page - 1 ) *window.perpage) + (index + 1)})).appendTo('#list_table_body')

  initUpload:(e)->
    $('#file_upload').fileupload
      autoUpload: false ,
      add: (er, data) ->
        data.url = '/uploads/upload_files';
        $('.upload-file').on('click', (e) ->
          data.submit()
          $('.upload-file').text('Uploading')
          e.preventDefault()
        )
      ,
      done: (e, data) ->
        if(data)
          $('.record-checked').text(data.result + ' record checked').show()
        $('.upload-file').text('Upload').attr("disabled", true)

  checkFile:() ->
    $('.input-pload-file').on('change', () ->
      validExts = new Array( ".xlsx")
      fileExt = this.value
      filePath = fileExt
      fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
      if (validExts.indexOf(fileExt) < 0)
        alert("Invalid file selected, valid files are of " + validExts.toString() + " types.")
        $('.preview-path').val('')
        $('.upload-file').attr("disabled", true)
        return false;

      else
        $('.preview-path').val(filePath)
        $('.upload-file').removeAttr('disabled' )
        return true

    )

  controlPanel:(e) ->
    $('.pagination').find('.prev').addClass('disable')
    $('.pagination').find('.first').addClass('disable')
    self = @
    page = 1
    user_collection = new Stuffilike.Collections.Users()
    user_collection.url = '/users/admin_user?page=' + page
    user_collection.fetch({success:(user_collection, data) ->
      $('#list_table_body').empty()
      count_user = data.users.length
      if count_user > 0
        $('.show-number-user').find('.n-all').text(data.total)
        total_page = Math.floor(data.total / window.perpage)
        total_page = total_page + 1 if (data.total / window.perpage) > total_page
        $('.pagination').find('.last').attr('data-id',total_page)
        $('.pagination').find('.next').attr('data-id',page + 1)
        if data.total < window.perpage
          $('.pagination').addClass('disable')
        users = new Stuffilike.Collections.Users(data.users)
        users.forEach(self.addOne,@)
        self.initPaginatePage(total_page)
    })

  featureStoreTab:() ->
    store = new Stuffilike.Views.AdminFeature()
    store.renderCreate()
    @showListStore()

  showListStore:() ->
    self = @
    store_collection = new Stuffilike.Collections.Items()
    store_collection.url = '/items/get_feature_stores'
    store_collection.fetch({success:(store_collection, data) ->
      if store_collection.length > 0
        store_collection.models.forEach(self.addStoreView,@)


    })

  addStoreView:(model) ->
    store_view = new Stuffilike.Views.FeatureStore({model: model})
    store_view.render()

  paginate:(e) ->
    $(e.currentTarget).parents('.ul-paginate').find('.paginate.active').removeClass('active')
    $(e.currentTarget).addClass('active')
    page = parseInt($(e.currentTarget).attr('data-id'))
    $('#list_table_body').attr('data-id', page)
    self = @
    user_collection = new Stuffilike.Collections.Users()
    user_collection.url = '/users/admin_user?page=' + page
    user_collection.fetch({success:(user_collection, data) ->
      $('#list_table_body').empty()
      count_user = data.users.length
      if count_user > 0
        $('.pagination').find('.next').attr('data-id',page + 1)
        $('.pagination').find('.prev').attr('data-id',page - 1) if page > 1
        if count_user + (page - 1) * window.perpage  == data.total
          $('.pagination').find('.next').addClass('disable')
          $('.pagination').find('.last').addClass('disable')
        else
          $('.pagination').find('.next').removeClass('disable')
          $('.pagination').find('.last').removeClass('disable')

        if page == 1
          $('.pagination').find('.prev').addClass('disable')
          $('.pagination').find('.first').addClass('disable')
        else
          $('.pagination').find('.prev').removeClass('disable')
          $('.pagination').find('.first').removeClass('disable')
        users = new Stuffilike.Collections.Users(data.users)
        users.forEach(self.addOne,@)
    })

  initPaginatePage:(pageNumber) ->
    max = 3
    max = pageNumber  if pageNumber < max
    for i in [0..max]
      if i == 0
        html = '<li class="tab-pag"><a class="paginate active" data-id="' + (i + 1) + '">' + (i + 1) + '</a></li>'
      else if i == max
        html = '<li class="tab-pag"><a class="paginate end" data-id="' + (i + 1) + '">' + (i + 1) + '</a></li>'
      else
        html = '<li class="tab-pag"><a class="paginate" data-id="' + (i + 1) + '">' + (i + 1) + '</a></li>'
      $('.ul-paginate').append(html)
    html = '<li class="tab-pag doc"><a >...</a></li> <li class="tab-pag"><a class="paginate next" data-id="2">Next</a></li><li class="tab-pag"><a class="paginate last" data-id="' + pageNumber + '">Last</a></li>'
    $('.ul-paginate').append(html)

  changeUpPaginateNav:(e) ->
    pageNumber = parseInt($('.paginate.last').attr('data-id'))
    current_page = parseInt($(e.currentTarget).attr('data-id'))
    min = current_page - 1
    if pageNumber > current_page
      $('.ul-paginate').find("li.tab-pag").remove()
      max = current_page + 3
      max = pageNumber - 1  if pageNumber <= max
      html = '<li class="tab-pag doc"><a >...</a></li>'
      $('.ul-paginate').append(html)
      for i in [min..max]
        if i == max
          html = '<li class="tab-pag"><a class="paginate end" data-id="' + (i + 1) + '">' + (i + 1) + '</a></li>'
        else
        if i ==  min
          html = '<li class="tab-pag"><a class="paginate start" data-id="' + (i + 1) + '">' + (i + 1) + '</a></li>'
        else
          html = '<li class="tab-pag"><a class="paginate" data-id="' + (i + 1) + '">' + (i + 1) + '</a></li>'
        $('.ul-paginate').append(html)
      if max = pageNumber
        html = ' <li class="tab-pag"><a class="paginate next" data-id="' + (min + 2) + '">Next</a></li><li class="tab-pag"><a class="paginate last" data-id="' + pageNumber + '">Last</a></li>'
      else
        html = '<li class="tab-pag doc"><a >...</a></li> <li class="tab-pag"><a class="paginate next" data-id="2">Next</a></li><li class="tab-pag"><a class="paginate last" data-id="' + pageNumber + '">Last</a></li>'
      $('.ul-paginate').append(html)
      $('.ul-paginate').find('.paginate[data-id="' + current_page + '"]').addClass('active')

  changeDownPaginateNav:(e) ->
    pageNumber = $('.paginate.last').attr('data-id')
    current_page = parseInt($(e.currentTarget).attr('data-id'))
    $('.ul-paginate').find("li.tab-pag").remove()
    min = current_page - 3
    min = 0 if min < 0
    max = current_page + 1
    for i in [min..max]
      if i == min && i > 0
        html = '<li class="tab-pag doc"><a >...</a></li><li class="tab-pag"><a class="paginate start" data-id="' + (i + 1) + '">' + (i + 1) + '</a></li>'
      else
        html = '<li class="tab-pag"><a class="paginate" data-id="' + (i + 1) + '">' + (i + 1) + '</a></li>'
      $('.ul-paginate').append(html)
    html = '<li class="tab-pag doc"><a >...</a></li> <li class="tab-pag"><a class="paginate next" data-id="' + (current_page - 1) + '2">Next</a></li><li class="tab-pag"><a class="paginate last" data-id="' + pageNumber + '">Last</a></li>'
    $('.ul-paginate').append(html)
    $('.ul-paginate').find('.paginate[data-id="' + current_page + '"]').addClass('active')
