class Stuffilike.Views.FeatureStore extends Backbone.View
  el: '#featured_stores'
  template_preview: JST['admin/feature_store_preview']


  initialize:(opts) ->
    @model = opts.model

  render:() ->
    @$('.list-preview-store').append(@template_preview({model: @model}))


