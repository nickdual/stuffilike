class Stuffilike.Views.AdminFeature extends Backbone.View
  el: '#featured_stores'
  template_create: JST['admin/feature_store_create']
  template_upload: JST['admin/feature_store_up_image']

  events:
    'click .btn-create-submit' : 'submitCreate'
    'click .btn-create-cancel' : 'cancelCreate'
    'click .icon-remove' : 'removeFeatureStore'
    'click .skip-this-step' : 'backToCreate'
    'click .btn-done-upload' : 'backToCreate'

  renderCreate:() ->
    @$('.wrap-form-store').html(@template_create())
    @validate()

  renderUploadImage:(model) ->
    @$('.wrap-form-store').html(@template_upload({model: model}))
    @initAddImage()

  initAddImage:() ->
    obj = @
    store_id = @$('.update-image').attr('data-id')
    @$("#add_image").fileupload
      autoUpload: true
      url: '/uploads/photo/' + store_id
      start:(e) ->

      done: (e, data) ->
        photo = new Stuffilike.Models.Photo()
        photo.set('link',data.result.url)
        photo.set('thumb',data.result.thumb)
        photo.url = '/items/photo_update/' + store_id
        photo.set('_id', data.result._id)
        photo.save(null,{success:(model, res) ->
          view = new  Stuffilike.Views.Photo({model: model})
          view.renderStream()
          img = self.$('.preview-store').find('img')
          if !img.hasClass('exists')
            img.attr('src',model.get('url')).addClass('exists')
        })

  validate:(e) ->
    @$('#create_store').validate
      errorClass: 'error'
      rules:
        "item[item_name]":
          required: true
        "item[description]":
          required: true
        "item[price]":
          required: true
        "item[merchant]" :
          required : true
        "item[brand]":
          required: true
        "item[link]":
          required: true
        "item[category]":
          required: true
        "item[referral_fee]":
          required: true


  submitCreate:(e) ->
    self = @
    e.preventDefault()
    target = $(e.currentTarget)
    form = target.parents('form#create_store')
    if @$('.list-preview-store').find('li.item').length < 4
      @$('.warning-create').hide()
      if @$('#create_store').validate().form()
        name = form.find("input[name='item[item_name]']").val()
        description = form.find("textarea[name='item[description]']").val()
        price = form.find("input[name='item[price]']").val()
        merchant = form.find("input[name='item[merchant]']").val()
        link = form.find("input[name='item[link]']").val()
        category = form.find("input[name='item[category]']").val()
        brand = form.find("input[name='item[brand]']").val()
        referral_fee = form.find("input[name='item[referral_fee]']").val()
        item = new Stuffilike.Models.Item()
        item.save({item:{'name': name, 'description': description, 'price': price, 'merchant': merchant, 'link': link, 'brand': brand, 'referral_fee' : referral_fee, 'category': category}},
          {success:(item, res) ->
            self.renderUploadImage(item)
            store_view = new Stuffilike.Views.FeatureStore({model: item})
            store_view.render()
          }
          ,{error:(e) -> }
        )
    else
      @$('.warning-create').show()

  removeFeatureStore:(e) ->
    e.preventDefault()
    target = $(e.currentTarget)
    item_id = target.parents('li.item').attr('data-id')
    item = new Stuffilike.Models.Item()
    if confirm 'Are you sure?'
      item.url = '/items/'+ item_id
      Backbone.sync 'delete', item,
        success: (data) ->
          target.parents('li.item[data-id="' + item_id + '"]').remove()
        error:(e) ->
          console.log 'er'

  backToCreate:(e) ->
    @renderCreate()