class Stuffilike.Views.ItemShow extends Backbone.View
  className: 'item'
  template_show: JST['items/item_show']
  template_edit: JST['items/item_edit']

  initialize:(opts) ->
    @model = opts.model
    @self = opts.self

  events:
    'click .delete-image-icon' : 'deleteImageItem'
    'click .edit-item-btn' : 'updateItem'
    'click .add_item_btn' : 'addToStore'
    'click .remove_item_btn' : 'removeToStore'

  renderShow:() ->
    $(@el).html(@template_show({model: @model,self: @self}))
    @

  renderEdit:() ->
    $(@el).html(@template_edit({model: @model}))
    @$("#item_images_carousel").carousel(
      interval: false
    )
    @initAddImage()
    @initTextareaResize()
    @

  initAddImage:() ->
    obj = @
    @$("#add_image").fileupload
      autoUpload: true
      url: '/uploads/photo/' + @model.get('_id').$oid
      start:(e) ->

      done: (e, data) ->
        photo = new Stuffilike.Models.Photo()
        photo.set('link',data.result.url)
        photo.set('thumb',data.result.thumb)
        photo.url = 'items/photo_update/' + obj.model.get('_id').$oid
        photo.set('_id', data.result._id)
        photo.save(null,{success:(model, res) ->
          view = new  Stuffilike.Views.Photo({model: model})
          view.removeDefault()
          view.renderGallery()
          view.renderGalleryThumb()
          view.renderStream()
        })


  initTextareaResize:() ->
    @$('#item_description').autosize()

  deleteImageItem:(e) ->
    obj = @
    li_photo = $(e.currentTarget).parents('li')
    item_id = li_photo.parents('ul#stream_photo').attr('data-id')
    photo_id = li_photo.attr('data-id')
    photo = new Stuffilike.Models.Photo()
    if confirm 'Are you sure?'
      photo.url = 'items/'+ item_id + '/delele_photo/' + photo_id
      Backbone.sync 'delete', photo,
        success: (data) ->
          p_st = $('#image_stream').find('li[data-id=' + data._id.$oid + ']')
          p_st.remove() if p_st
          p_ga = $('#carousel_collection').find('div.item[data-id=' + data._id.$oid + ']')
          p_ga.remove() if p_ga
          p_th = $('#carousel_thumb').find('li.img-thumb[data-id=' + data._id.$oid + ']')
          p_th.remove() if p_th
          obj.setDefaultPhoto()
        error:(e) ->
          console.log 'er'

  setDefaultPhoto:(e) ->
    p_ga = $('#carousel_collection').find('div.item')
    p_th = $('#carousel_thumb').find('li.img-thumb')
    if p_ga.length == 0 && p_th.length == 0
      model = new Stuffilike.Models.Photo()
      view = new  Stuffilike.Views.Photo({model: model})
      view.renderGallery()
      view.renderGalleryThumb()

  updateItem:(e) ->
    @$('.edit-item-btn').attr("disabled", true)
    name = @$('input[name="item[name]"]').val()
    description = @$('textarea[name="item[description]"]').text()
    price = @$('input[name="item[price]"]').val()
    merchant = @$('input[name="item[merchant]"]').val()
    brand = @$('input[name="item[brand]"]').val()
    referral_fee = @$('input[name="item[referral_fee]"]').val()
    link = @$('input[name="item[link]"]').val()
    category = @$('input[name="item[category]"]').val()
    @model.set('name', name)
    @model.set('description', description)
    @model.set('referral_fee', referral_fee)
    @model.set('price', price)
    @model.set('link', link)
    @model.set('brand', brand)
    @model.set('merchant', merchant)
    @model.set('category', category)
    self = @
    @model.save(null,{success:(model, res) ->
      if res.status
        self.$('.edit-item-btn').removeAttr('disabled' )
    })

  addToStore:(e) ->
    if Stuffilike.currentUser
      $('.rq-login').hide()
      self = @
      item_id = $(e.currentTarget).parents('span').attr('data-id')
      item = new Stuffilike.Models.Item()
      item.url = '/items/add_item_to_store/' + item_id
      item.save(null,{success:(model, res) ->
        html = '<a class="btn btn-large add_to_warehouse_button remove_item_btn delete">Delete In My Store</a>'
        $(e.currentTarget).parents('span').html(html)
      })
    else
      $('.rq-login').show()

  removeToStore:(e) ->
    self = @
    item_id = $(e.currentTarget).parents('span').attr('data-id')
    item = new Stuffilike.Models.Item()
    item.url = '/items/remove_item_to_store/' + item_id
    item.save(null,{success:(model, res) ->
      html = '<a class="btn btn-large add_to_warehouse_button add_item_btn add"><i class="icon-plus" style="margin-right: 10px"></i>Add To My Store</a>'
      $(e.currentTarget).parents('span').html(html)
    })