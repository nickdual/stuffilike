class Stuffilike.Views.Item extends Backbone.View
  className: 'item'
  template: JST['items/item_view']
  template_item_store: JST['my_stores/item_view_store']

  initialize:(opts) ->
    @model = opts.model
    @model.on('remove', @destroy, @)
    @admin = Stuffilike.currentRoleAdmin
    @self = opts.self

  render:() ->
    @el = $(@template({model: @model, admin :@admin})).appendTo('#list_items')
    @

  ItemStore:() ->
    @el =  $(@template_item_store({model: @model, self: @self  })).appendTo('#list_items')
    @

  destroy: ->
    @model.url = '/items/' + @model.get('_id').$oid



