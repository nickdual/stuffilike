class Stuffilike.Views.PhotoLists extends Backbone.View
  el: '.item-edit'

  initialize: ->
    @collection.on('add',@addOne, @)
    @collection.on('reset', @addAll,@)

  addOne: (model) ->
    view = new  Stuffilike.Views.Photo({model: model})
    view.renderGallery()
    view.renderGalleryThumb()
    view.renderStream()

  addAll: ->
    $('#image_stream').empty()
    $('#carousel_collection').empty()
    $('#carousel_thumb').empty()
    @collection.forEach(@addOne, @)

  render: ->
    @addAll()
    @
