class Stuffilike.Views.Photo extends Backbone.View
  className: 'photo'
  photoGallery: JST['items/photo_gallery']
  photoGalleryThumb: JST['items/photo_gallery_thumb']
  photoStream: JST['items/photo_stream']
  photoPreview: JST['items/photo_preview']

  initialize:(opts) ->
    @model = opts.model
    @model.on('remove', @destroy, @)

  renderPreview:(_el) ->
    @el = $(@photoPreview({model: @model})).appendTo(_el)
    @

  renderGallery:() ->
    @el = $(@photoGallery({model: @model})).appendTo('#carousel_collection')
    @setActive()
    @

  renderGalleryThumb:() ->
    li_thumb = $('#carousel_thumb').find('li.img-thumb')
    @el = $(@photoGalleryThumb({model: @model, order: li_thumb.length})).appendTo('#carousel_thumb')
    @

  renderStream:() ->
    @el = $(@photoStream({model: @model})).appendTo('#image_stream')
    item_id =  $(@el).parents('ul#stream_photo').attr('data-id')
    @initEditPhoto(item_id)
    @

  setActive:() ->
    $('#carousel_collection').children('div.item:first').addClass('active')

  removeDefault:() ->
    item = $('#carousel_collection').children('div.item:first')
    if item.hasClass('default')
      item.remove()
    item_thumb =  $('#carousel_thumb').children('li:first')
    if item_thumb.hasClass('default')
      item_thumb.remove()

  refreshPhoto:(model) ->
    photo_id = model.get('_id').$oid
    p_gar = $('#carousel_collection').find('div[data-id="' + photo_id+ '"]')
    if p_gar
      p_gar.find('img').attr('src', model.get('url'))
    p_thumb = $('#carousel_thumb').find('li[data-id="' + photo_id+ '"]')
    if p_thumb
      p_thumb.find('img').attr('src', model.get('thumb'))
    p_stream = $('#image_stream').find('li[data-id="' + photo_id+ '"]')
    if p_stream
      p_stream.find('img').attr('src', model.get('thumb'))

  initEditPhoto:(item_id) ->
    obj = @
    $("#edit_image_item_#{@model.cid}").fileupload
      autoUpload: true
      url: '/uploads/photo/' + @model.get('_id').$oid + '/' + item_id
      done: (e, data) ->
        obj.model.url = '/items/photo_update/' + item_id
        obj.model.set('link',data.result.url)
        obj.model.set('thumb',data.result.thumb)
        obj.model.save(null,{success:(model, res) ->
          obj.refreshPhoto(model)
        })