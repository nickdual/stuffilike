class Stuffilike.Views.ListItems extends Backbone.View
  el: '#list_items'

  initialize:(opts) ->
    @scrollPage()
    @collection.on('add',@addOne, @)
    @collection.on('reset', @addAll,@)
    @store = opts.store
    @self = opts.self

  events:
    'click .btn-delete-item' : 'deleteItem'
    'click .add_item_btn' : 'addToStore'
    'click .remove_item_btn' : 'removeToStore'

  addOne: (model) ->
    view = new Stuffilike.Views.Item({model: model, self: @self})
    if @store
      view.ItemStore()
    else
      view.render()
    photo_collection =  model.get('photoes')
    _el = $(view.el).find('#photo_preview')
    if photo_collection
      length = photo_collection.length
      switch true
        when length >= 2
          photo1 = photo_collection.at(0)
          photo2 = photo_collection.at(1)
          photo_preview = new Stuffilike.Views.Photo(model: photo1)
          photo_preview.renderPreview(_el)
          photo_preview2 = new Stuffilike.Views.Photo(model: photo2)
          photo_preview2.renderPreview(_el)
        when length == 1
          photo = photo_collection.at(0)
          photo_preview = new Stuffilike.Views.Photo(model: photo)
          photo_preview.renderPreview(_el)
          $(_el).removeClass('image-view')
          $(_el).addClass('image-preview')
        when length == 0
          photo = new Stuffilike.Models.Photo()
          photo_preview = new Stuffilike.Views.Photo(model: photo)
          photo_preview.renderPreview(_el)
          $(_el).removeClass('image-view')
          $(_el).addClass('image-preview')
    else
      photo = new Stuffilike.Models.Photo()
      photo_preview = new Stuffilike.Views.Photo(model: photo)
      photo_preview.renderPreview(_el)
      $(_el).removeClass('image-view')
      $(_el).addClass('image-preview')

  addAll: ->
    if @collection.length > 0
      $('#list_items').empty()
      @collection.forEach(@addOne, @)

  addAllMore: ->
    @collection.forEach(@addOne, @)

  render: ->
    @addAll()
    @

  renderMore: ->
    @addAllMore()
    @

  deleteItem:(e) ->
    cid = $(e.currentTarget).attr('data-id')
    model = @collection.get(cid)
    if confirm 'Are you sure to delete this item?'
      @collection.remove(cid)
      model.destroy()
      @$("li[data-id=" + cid + "]").parent('.item').remove()

  addToStore:(e) ->
    self = @
    item_id = $(e.currentTarget).parents('.shop-actions').attr('data-id')
    item = new Stuffilike.Models.Item()
    item.url = '/items/add_item_to_store/' + item_id
    item.save(null,{success:(model, res) ->
      if res.status
        ul = $(e.currentTarget).parents('ul#list_items')
        $(e.currentTarget).parents('div.item').remove()
        if ul.find('div.item').length == 0
          label = '<label class="no-items">There are No Items or all Items has added to your store</label>'
          ul.html(label)
        else if ul.find('div.item').length < 8
          console.log 'a'
          loading = $('body').find('.loading-more')
          if !loading.hasClass('loading') && !loading.hasClass('end')
            loading.addClass('loading')
            page = loading.attr('data-value')
            loading.show()
            items = new Stuffilike.Collections.Items()
            items.url = '/items/index/' + page
            items.fetch({success:(collection, res) ->
              self.collection = collection
              self.renderMore()
              loading.attr('data-value', parseInt(page) + 1)
              loading.hide()
              loading.removeClass('loading')
              if collection.length < 8
                loading.addClass('end')
            })
    })

  removeToStore:(e) ->
    self = @
    item_id = $(e.currentTarget).parents('.shop-actions').attr('data-id')
    item = new Stuffilike.Models.Item()
    item.url = '/items/remove_item_to_store/' + item_id
    item.save(null,{success:(model, res) ->
      if res.status
        ul = $(e.currentTarget).parents('ul#list_items')
        $(e.currentTarget).parents('div.item').remove()
        if ul.find('div.item').length == 0
          label = '<label class="no-items">There are No Items in your store</label>'
          ul.html(label)
        else if ul.find('div.item').length < 8
          loading = $('body').find('.loading-more')
          if !loading.hasClass('loading') && !loading.hasClass('end')
            loading.addClass('loading')
            page = loading.attr('data-value')
            loading.show()
            username = $('body').find('#my_store').attr('data-value')
            items = new Stuffilike.Collections.Items()
            items.url = '/items/' + page+ '/store/' + username
            items.fetch({success:(collection, res) ->
              self.collection = collection
              self.renderMore()
              loading.attr('data-value', parseInt(page) + 1)
              loading.hide()
              loading.removeClass('loading')
              if collection.length < 8
                loading.addClass('end')
            })
    })

  scrollPage: () ->
    self = @
    $(window).scroll( () ->
      min_scroll = 10
      if self.store
        max_scroll = 840
      else
        max_scroll = 505
      scrollTop = document.documentElement.scrollTop || document.body.scrollTop
      scrollBottom = $(document).height() - $(window).height() - $(window).scrollTop()
      if(scrollTop > max_scroll && scrollBottom < min_scroll )
        loading = $('body').find('.loading-more')
        if !loading.hasClass('loading') && !loading.hasClass('end')
          loading.addClass('loading')
          page = loading.attr('data-value')
          loading.show()
          username = $('body').find('#my_store').attr('data-value')
          items = new Stuffilike.Collections.Items()
          if self.store
            items.url = '/items/' + page+ '/store/' + username
          else
            items.url = '/items/index/' + page
          items.fetch({success:(collection, res) ->
            self.collection = collection
            self.renderMore()
            loading.attr('data-value', parseInt(page) + 1)
            loading.hide()
            loading.removeClass('loading')
            if collection.length < 8
              loading.addClass('end')
          }))