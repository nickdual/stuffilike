class Stuffilike.Views.SignUp extends Backbone.View
  el: "#users_page"
  template: JST['users/sign_up']

  events:
   "click #sign_up": "save"

  initialize: ->
    @model = new Stuffilike.Models.User()

  render: ->
    @$el.html(@template({model: @model}))
    @$('#charity').autosize()
    @validate()
    @$('.select-charity').chosen(
      disable_search: true
    )

  validate: ->
    @$("#form_sign_up").validate
      errorClass: "error"
      rules:
        "user[first_name]":
          required: true
        "user[last_name]":
          required: true
        "user[email]":
          required: true
        "user[username]":
          required: true
        "user[password]":
          required: true
          maxlength: 36
          minlength: 8

  save: (e) ->
    target = $(e.currentTarget)
    self = @
    if @$("#form_sign_up").validate().form()
      target.val("Creating")
      email =  @$("#user_email").val()
      first_name = @$("#user_first_name").val()
      last_name = @$("#user_last_name").val()
      password = @$("#user_password").val()
      username = @$("#username").val()
      charity = @$("#charity").val()
      radio = $(@el).find('input[name="banner"]')
      banner_img = ''
      radio.each((e) ->
        if jQuery(this).is(':checked')
          class_banner = $(this).attr('data-class')
          banner_img = $(".#{class_banner}").css('background-image').replace('url("', '').replace('url(', '').replace('")','').replace(')','')
      )
      @model.set("password_confirmation", @$("#user_password").val())
      @model.save( {user: {email: email, password: password, password_confirmation: password, first_name: first_name, last_name: last_name, username: username , charity: charity, background_banner: banner_img}},{
        success: (userSession, response) ->
          if response.status == "200"
            $("#sign_up_success").show()
            Stuffilike.currentUser = new Stuffilike.Models.User(response.user)
            window.location.href = "/"
          else
            $("#sign_up_error").html(response.message).show()
            target.val("Save")
      },
        error: (userSession, response) ->
      )
    e.preventDefault()