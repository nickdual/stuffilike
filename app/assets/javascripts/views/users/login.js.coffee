class Stuffilike.Views.Login extends Backbone.View
  el: "#users_page"
  template: JST['users/login']

  initialize: ->
    @model = new Stuffilike.Models.User()

  events:
    'click #sign_in': 'login'
    'click #forget_password' : 'reset_pass_form'
    'keypress #user_password' : 'enterLogin'
    'keypress #user_email' : 'enterLogin'

  render: ->
    @$el.html(@template({model: @model}))
    @validate()

  validate: ->
    @$("#form_sign_in").validate
      errorClass: "error"
      rules:
        "user[email]":
          required: true
        "user[password]":
          required: true
          maxlength: 256
          minlength: 8

  enterLogin:(e) ->
    if (e.keyCode == 13)
      @login(e);

  login: (e) ->
    el = $(@el)
    e.preventDefault()
    if @$("#form_sign_in").validate().form()
      email = @$('#user_email').val()
      password = @$('#user_password').val()
      remember_me = 0
      remember_me = 1 if @$('#user_remember_me').is(':checked')
      el.find('#sign_in').button('loading')
      @model.url = "/users/sign_in"
      @model.save({user: {email: email, password: password, remember_me: remember_me}},
        success: (userSession, response) ->
          if response.success == true
            console.log response
            Stuffilike.currentUser = new Stuffilike.Models.User(response.user)
            if response.admin
              window.location.href = "items/#items"
            else
              window.location.href = "/" + Stuffilike.currentUser.get('username')
          else if response.success == false && response.flag == 'email_error'
            el.find("#sign_in_error").html(response.message).show()
          else
            el.find("#sign_in_error").html(response.message).show()
          el.find('#sign_in').button('reset')
        ,error: (userSession,response)->
          el.find("#sign_in_error").html("Email or password not correct").show()
          el.find('#sign_in').button('reset')

      )


