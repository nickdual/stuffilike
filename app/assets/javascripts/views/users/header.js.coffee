class Stuffilike.Views.Header extends Backbone.View
  el: "#user_info"
  template_logout: JST['users/user_info']
  template: JST['users/header_sign_in']

  initialize: (opts) ->
    @model = opts.model

  render: ->
    if @model
      @$el.html(@template_logout({model: @model}))
    else
      @$el.html(@template())


