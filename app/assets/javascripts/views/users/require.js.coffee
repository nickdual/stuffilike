class Stuffilike.Views.Require extends Backbone.View
  el: "#users_page"

  template: JST['users/require']

  initialize:(opts) ->
    if opts.model
      @model = opts.model
    else
      @model = new Stuffilike.Models.User()

  events:
    'keyup #username' : 'update_url'
    'click #done_resg' :'save'

  render: ->
    @$el.html(@template({model: @model}))
    @validate()
    @$('.select-charity').chosen(
      disable_search: true
    )

  validate: ->
    @$("#form_require").validate
      errorClass: "error"
      rules:
        "user[username]":
          required: true


  update_url:(e) ->
    text = $(e.currentTarget).val()
    @$('#name-span').text(text)

  save: (e) ->
    target = $(e.currentTarget)
    if @$("#form_require").validate().form()
      username = @$("#username").val()
      charity = @$("#charity").val()
      radio = $(@el).find('input[name="banner"]')
      banner_img = ''
      radio.each((e) ->
        if jQuery(this).is(':checked')
          class_banner = $(this).attr('data-class')
          banner_img = $(".#{class_banner}").css('background-image').replace('url("', '').replace('url(', '').replace('")','').replace(')','')
      )
      @model.url = '/users/require_info'
      @model.save( {require: {username: username, charity: charity, banner_img: banner_img}},{
        success: (userSession, response) ->
          if response.status == "200"
            window.location.href = "/"
          else
            $("#require_error").html(response.message).show()
            target.val("Save")
      },
        error: (userSession, response) ->
      )
    e.preventDefault()