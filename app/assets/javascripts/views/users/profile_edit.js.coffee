class Stuffilike.Views.ProfileEdit extends Backbone.View
  el: "#profile_page"
  template: JST['users/user_info_edit']

  events:
   "click #submit_edit_btn": "editProfile"

  initialize:(opts) ->
    self = @
    @model = new Stuffilike.Models.User()
    @model.url = '/users/get_info'
    @model.fetch(
      {success:(model, res) ->
        self.$el.html(self.template({user: model}))
        self.$('#charity').autosize()
        self.validate()
        self.initEditAvatar()
        self.$('.select-charity').chosen(
          disable_search: true
        )
      }
    )

  validate: ->
    @$("#form_edit_profile").validate
      errorClass: "error"
      rules:
        "user[first_name]":
          required: true
        "user[last_name]":
          required: true
        "user[username]":
          required: true

  editProfile: (e) ->
    target = $(e.currentTarget)
    user_id = target.parents('#user_edit_row').attr('data-id')
    self = @
    if @$("#form_edit_profile").validate().form()
      target.text("Updating")
      first_name = @$("#first_name").val()
      last_name = @$("#last_name").val()
      username = @$("#username").val()
      charity = @$("#charity").val()
      @model.url = '/users/' + user_id
      @model.set('first_name', first_name)
      @model.set('last_name', last_name)
      @model.set('username', username)
      @model.set('charity', charity)
      Backbone.sync('update', @model ,
        success:(model, res)->
          if res.status == "200"
            Stuffilike.currentUser = new Stuffilike.Models.User(res.user)
            $("#update_success").show()
          else
            $("#update_success").addClass('error')
            $("#update_success").html(res.message).show()
          target.text("Submit Edit")
        ,
        error: ( e) ->
      )
    e.preventDefault()

  initEditAvatar:() ->
    obj = @
    @$('#edit_avatar').fileupload
      autoUpload: true
      url: '/uploads/avatar'
      start:(e) ->

      done: (e, data) ->
        if data.result
          avatar_link = data.result.avatar
          avatar = Stuffilike.currentUser.get('avatar')
          model = new Backbone.Model()
          model.url = '/users/update_avatar'
          model.set('avatar_link',avatar_link)
          model.save({},{success:(model) ->
            obj.$('.avatar-part').find('img.avatar').attr('src', model.get('avatar_link') )
          })