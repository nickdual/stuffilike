class Stuffilike.Routers.Users extends Backbone.Router
  routes:
    "sign_in": "sign_in"
    "require": "require"
    "sign_up": "sign_up"
    "forgotpassword": "reset"

  sign_in: () ->
    unless Stuffilike.currentUser
      view = new Stuffilike.Views.Login()
      view.render()
    else
      if Stuffilike.currentRoleAdmin
        window.location.href = "items/#items"
      else
        window.location.href = "/" + Stuffilike.currentUser.get('username')

  sign_up: () ->
    unless Stuffilike.currentUser
      view = new Stuffilike.Views.SignUp()
      view.render()
    else
      if Stuffilike.currentRoleAdmin
        window.location.href = "items/#items"
      else
        window.location.href = "/" + Stuffilike.currentUser.get('username')

  reset: () ->
    unless Stuffilike.currentUser
      view = new Stuffilike.Views.ResetPassword()
    else
      if Stuffilike.currentRoleAdmin
        window.location.href = "items/#items"
      else
        window.location.href = "/" + Stuffilike.currentUser.get('username')

  require:() ->
    @model = Stuffilike.currentUser
    require = new Stuffilike.Views.Require(model: @model)
    require.render()