class Stuffilike.Routers.Items extends Backbone.Router
  routes:
    "items" : 'listItems'
    'show/:itemId' :'show'
    'edit/:itemId' :'edit'

  initialize : =>
    window.scrollTo(0, 0)
    @collection = new Stuffilike.Collections.Items()

  listItems:() ->
    unless Stuffilike.currentUser
      window.location.href = "/users#sign_in"
    else
      $('#home').empty()
      $('#my_store').empty()
      $('.qtip').hide()
      $('#top-navigation').find('.nav').children('li').removeClass('active')
      $('#top-navigation').find('.nav').children('li.items-tab').addClass('active')
      item_page = new Stuffilike.Views.ItemPage()
      item_page.render()
      @collection.fetch({success :(collection, res) ->
        list_items = new Stuffilike.Views.ListItems({collection: collection, store: false})
        list_items.render()
      })

  show: (itemId) =>
      item_page = new Stuffilike.Views.ItemPage()
      item_page.render()
      $('body').find('#home').empty()
      $('body').find('#my_store').empty()
      $('body').find('#list_items').empty()
      $('body').find('.qtip').hide()
      $('#top-navigation').find('.nav').children('li').removeClass('active')
      $('#top-navigation').find('.nav').children('li.items-tab').addClass('active')

      item = new Stuffilike.Models.Item()
      item.url = '/items/' + itemId
      item.fetch({success:(model, data) ->
        self_bo = data.self
        new_item = new Stuffilike.Models.Item(data.item)
        item_show = new Stuffilike.Views.ItemShow({model: new_item, self: self_bo})
        $('#item_show').html(item_show.renderShow().el)
        photo_collection = new_item.get('photoes')
        if typeof(photo_collection) == 'undefined' || photo_collection.length == 0
          model = new Stuffilike.Models.Photo()
          view = new  Stuffilike.Views.Photo({model: model})
          view.renderGallery()
          view.renderGalleryThumb()
        else
          list_photo = new Stuffilike.Views.PhotoLists(collection: photo_collection)
          list_photo.render()
      })

  edit: (itemId) =>
    unless Stuffilike.currentUser
      window.location.href = "/users#sign_in"
    else
      if Stuffilike.currentRoleAdmin == true
        item_page = new Stuffilike.Views.ItemPage()
        item_page.render()
        $('body').find('#home').empty()
        $('body').find('#my_store').empty()
        $('body').find('#list_items').empty()
        $('body').find('.qtip').hide()
        $('#top-navigation').find('.nav').children('li').removeClass('active')
        $('#top-navigation').find('.nav').children('li.items-tab').addClass('active')
        item = new Stuffilike.Models.Item()
        item.url = '/items/' + itemId + '/edit'
        item.fetch({success:() ->
          item_show = new Stuffilike.Views.ItemShow({model: item})
          $('#item_show').html(item_show.renderEdit().el)
          photo_collection = item.get('photoes')
          if typeof(photo_collection) == 'undefined' || photo_collection.length == 0
            model = new Stuffilike.Models.Photo()
            view = new  Stuffilike.Views.Photo({model: model})
            view.renderGallery()
            view.renderGalleryThumb()
          else
            list_photo = new Stuffilike.Views.PhotoLists(collection: photo_collection)
            list_photo.render()
        })
      else
        window.location.href = "items/#items"

