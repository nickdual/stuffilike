// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require underscore
//= require backbone
//= require stuffilike
//= require_tree ../templates
//= require_tree ./models
//= require_tree ./collections
//= require_tree ./views
//= require_tree ./routers
//= require ./lib/upload/jquery.ui.widget
//= require ./lib/upload/jquery.iframe-transport
//= require ./lib/upload/jquery.fileupload.js
//= require ./lib/bootstrap
//= require ./lib/jquery-validate
//= require ./lib/jquery.qtip
//= require ./lib/backbone-nested.min
//= require ./lib/jquery.autosize.min
//= require ./lib/prettify
//= require ./lib/lightbox
//= require ./lib/chosen.jquery
//= require ./lib/main
//= require_tree .
