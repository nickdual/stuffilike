namespace :cj do

  desc "Sync cj data"
  task :sync => :environment do
    file_path = 'public/cj.xlsx'
    Cj.sync(file_path)
  end

end